import os
from flask_cors import CORS
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, render_template, jsonify, send_from_directory, request

# Create flask app
app = Flask(__name__)

# Add CORS autorization
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

# Configuration db
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'db.sqlite3')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# Move import here to avoid a bug with the migration process
from models import db, SNP, SNP2Phenotype2Ref, DISEASE_TRAIT, REFERENCE
# Init db
db.init_app(app)
migrate = Migrate(app, db)


def get_snp(id=None, field=None, value=None):
    ''' 
    Return an array of all the SNPs if no argument given, each in a dict.
    If an ID is given, return the SNP corresponding.
    If field and value are given, make the research and return a list of SNP containing
    the value in the field.
    ''' 

    # Return the SNP with the corresponding id 
    if id:
        res = SNP.query.filter_by(id=id)
    # Make Search 
    elif field and value:
        if field == 'Chrom':
            # If it's a numeric chromosome, search the exact value
            if value.isdigit():
                res = SNP.query.filter_by(Chrom=value)
            else:
                res = db.session.query(SNP).filter(SNP.Chrom.contains(value))
        elif field == 'Rsid':
            res = db.session.query(SNP).filter(SNP.Rsid.contains(value))
        elif field == 'Region':
            res = db.session.query(SNP).filter(SNP.Region.contains(value))    
        elif field == 'Context':
            res = db.session.query(SNP).filter(SNP.Context.contains(value))
    # Return all SNP
    else:
        res = SNP.query.all()

    # Format SNP to array of json
    array_json = []
    for r in res:
        # Delete '_variant' if r.Context != None
        context = r.Context.replace('_variant', '') if r.Context else r.Context
        # Recover SNP2Phenotype2Ref table 
        s = r.SNP2Phenotype2Ref[-1]
        d = DISEASE_TRAIT.query.filter_by(id=s.Disease_trait_id).first()
        json = {'Id': r.id, 'Chrom': r.Chrom, 'Chrom_pos': r.Chrom_pos,
            'Rsid': r.Rsid, 'Region': r.Region, 'Context': context,
            'Strongest_SNP_risk_allele': r.Strongest_SNP_risk_allele,
            'Risk_frequency': r.Risk_frequency, 'Pvalue': s.pvalue,
            'NegLog': s.neglog10pvalue, 'Phenotype': d.Name}
        array_json.append(json)
    return array_json


@app.route('/api/snp/')
@app.route('/api/snp/<int:id>')
def snp(id=None):
    if not id:
        return jsonify(get_snp())
    else:
        r = SNP.query.filter_by(id=id).first()
        r_dict = {'Chrom': r.Chrom, 'Chrom_pos': r.Chrom_pos,
            'Rsid': r.Rsid, 'Region': r.Region}
        same_rsid = SNP.query.filter_by(Rsid=r.Rsid)
        list_snp = []
        for snp in same_rsid:
            S2P2R = SNP2Phenotype2Ref.query.filter_by(Snp_id=snp.id).first()
            context = r.Context.replace('_variant', '') if r.Context else r.Context
            temp_dict = {'Id': snp.id, 'Context': context,
            'Strongest_SNP_risk_allele': snp.Strongest_SNP_risk_allele,
            'Risk_frequency': snp.Risk_frequency, 'Pvalue': snp.SNP2Phenotype2Ref[-1].pvalue,
            'NegLog': snp.SNP2Phenotype2Ref[-1].neglog10pvalue,
            'Pubmed_id': S2P2R.Reference.Pubmed_id, 'Journal': S2P2R.Reference.Journal,
            'Title': S2P2R.Reference.Title, 'First_author': S2P2R.Reference.First_author,
            'Sample': S2P2R.Reference.Sample, 'Date': S2P2R.Reference.Date,
            'Dis_id': S2P2R.Disease_trait.id, 'Disease_name': S2P2R.Disease_trait.Name,}
            list_snp.append(temp_dict)

        r_dict['snp'] = list_snp
        return jsonify(r_dict)


@app.route('/api/snpSearch/<string:field>/<value>')
def snp_search(field, value):
    result = get_snp(field=field, value=value)
    if not result:
        result = {'empty': True}
    return jsonify(result)


def get_phenotype(id=None, search_value=None):
    ''' 
    Return an array of all the Phenotypes if no argument given, each in a dict.
    If an ID is given, return the Phenotype corresponding.
    If search_value is given, make the research and return a list of Phenotype containing
    the search_value.
    ''' 

    # Return the Phenotype with the corresponding id 
    if id:
        results = DISEASE_TRAIT.query.filter_by(id=id)
    # Make Search 
    elif search_value:
        results = db.session.query(DISEASE_TRAIT)\
                    .filter(DISEASE_TRAIT.Name.contains(search_value))
    # Return all Phenotype
    else:
        results = DISEASE_TRAIT.query.all()

    array_json = []
    already_use = [] 
    for result in results:
        # Add only not used 
        if result.Name not in already_use:
            array_json.append({"Id": result.id, "Disease_trait": result.Name})
            already_use.append(result.Name)
        else:
            continue

    return array_json


@app.route('/api/phenotype/')
@app.route('/api/phenotype/<int:id>')
def phenotype(id=None):
    if not id:
        return jsonify(get_phenotype())
    else:
        res = SNP2Phenotype2Ref.query.filter_by(Disease_trait_id=id).first()
        list_article = []
        list_SNP = []
        for pheno in DISEASE_TRAIT.query.filter_by(Name=res.Disease_trait.Name):
            nr =  SNP2Phenotype2Ref.query.filter_by(Disease_trait_id=pheno.id).first()
            temp_article = {'Title': nr.Reference.Title, 'PubmedId': nr.Reference.Pubmed_id}
            temp_snp = {'Rsid': nr.Snp.Rsid, 'Id': nr.Snp.id}
            if not temp_article in list_article:
                list_article.append(temp_article)
            if not temp_snp in list_SNP:
                list_SNP.append(temp_snp)
        json = {'Name': res.Disease_trait.Name, 'Article': list_article, 'Snp': list_SNP}
        return jsonify(json)


@app.route('/api/phenotypeSearch/<string:value>')
def phenotype_search(value):
    # Research 
    json = get_phenotype(search_value=value)
    # If no result set json to empty 
    if not json:
        json = {"empty": True}
    
    return jsonify(json)


if __name__ == "__main__":
    app.run(host='127.0.0.1')
    