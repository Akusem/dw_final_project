import os
import sys
import csv
import datetime
from server import db
from flask import Flask
from models import SNP, SNP2Phenotype2Ref, DISEASE_TRAIT, REFERENCE

def create_app():
    ''' Setup an app and init the db '''
    app = Flask(__name__)
    basedir = os.path.abspath(os.path.dirname(__file__))
    app.config['SQLALCHEMY_DATABASE_URI'] =\
        'sqlite:///' + os.path.join(basedir, 'db.sqlite3') 
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)
    return app

def main():

    # Initialize app to use its context (necessary to use the db outside of a views)
    app = create_app()
    ctx = app.app_context()
    ctx.push()

    # Read file 
    file_name = sys.argv[1]
    file = csv.reader(open(file_name, 'r'), delimiter='\t') 
    db.create_all()

    next(file)  # pass header
    for line in file:
        # get years, month and days to use datetime later
        y, m, d = [int(d) for d in line[2].split('-')]
        # Replace NR by None to have consistent n/a in all field.
        if line[15] == 'NR':
            line[15] = None
        # replace empty string by None to fill db with n/a
        for i, el in enumerate(line): 
            if el == '':
                line[i] = None
        
        ref = REFERENCE(Pubmed_id=line[0], Journal=line[3], First_author=line[1], 
                        Title=line[5], Sample=line[7], Date=datetime.date(y, m, d))
        dis = DISEASE_TRAIT(Name=line[6])

        
        snp = SNP(Chrom=line[9], Chrom_pos=line[10], Rsid=line[12], Region=line[8],
                    Context=line[14], Strongest_SNP_risk_allele=line[11][-1], 
                    Risk_frequency=line[15])

        db.session.add(ref)
        db.session.add(dis)
        db.session.add(snp)
        db.session.commit()

        # snp2phe2ref is done after the other table are commit as their id is define when wrote in db
        snp2phe2ref = SNP2Phenotype2Ref(Snp_id=snp.id, Disease_trait_id=dis.id, Reference_id=ref.id,
                            pvalue=float(line[16]), neglog10pvalue=float(line[17]))
        db.session.add(snp2phe2ref)
        db.session.commit()

    ctx.pop()



if __name__ == "__main__":
    main()