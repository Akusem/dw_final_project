# from app import db
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class SNP(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Chrom = db.Column(db.String(5))
    Chrom_pos = db.Column(db.String)
    Rsid = db.Column(db.String(15))
    Region = db.Column(db.String(10))
    Context = db.Column(db.String(30))
    Strongest_SNP_risk_allele = db.Column(db.String(1))
    Risk_frequency = db.Column(db.String)
    SNP2Phenotype2Ref = db.relationship("SNP2Phenotype2Ref", back_populates="Snp")

    def __repr__(self):
        return str(self.Rsid)

class REFERENCE(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Pubmed_id = db.Column(db.String)
    Journal = db.Column(db.String(42))
    First_author = db.Column(db.String(42))
    Title = db.Column(db.String(200))
    Sample = db.Column(db.String(200))
    Date = db.Column(db.Date)
    SNP2Phenotype2Ref = db.relationship("SNP2Phenotype2Ref", back_populates="Reference")

    def __repr__(self):
        return self.Title

class DISEASE_TRAIT(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.Text)
    SNP2Phenotype2Ref = db.relationship("SNP2Phenotype2Ref", back_populates="Disease_trait")

    def __repr__(self):
        return self.Name

class SNP2Phenotype2Ref(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Snp_id = db.Column(db.Integer, db.ForeignKey('SNP.id'))
    Disease_trait_id = db.Column(db.Integer, db.ForeignKey('DISEASE_TRAIT.id'))
    Reference_id = db.Column(db.Integer, db.ForeignKey('REFERENCE.id'))
    pvalue = db.Column(db.Float)
    neglog10pvalue = db.Column(db.Float)
    Snp = db.relationship("SNP", back_populates="SNP2Phenotype2Ref")
    Disease_trait = db.relationship("DISEASE_TRAIT", back_populates="SNP2Phenotype2Ref")
    Reference = db.relationship("REFERENCE", back_populates="SNP2Phenotype2Ref")

    def __repr__(self):
        return str(self.id)+' '+str(self.Snp_id)+' '+str(self.Disease_trait_id)\
                +' '+str(self.Reference_id)+' '+str(self.pvalue)+' '+str(self.neglog10pvalue)
