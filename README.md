# Final Web Development Project of Master 2 B:DLAD by N.ZWEIG

The website SNP Database is available at [https://client.akusem.now.sh](https://client.akusem.now.sh)
for testing.

As I have already done and validated this course 2 years ago, I have decided to
create it with a more up-to-date technology, [React.js](https://reactjs.org/).

React is a client-side Javascript framework principaly dedicated to the creation
of a single-page-application, and it's also with 
[Angular and VueJs the most wided used Javascript framework](https://www.jetbrains.com/lp/devecosystem-2019/javascript/).

### Structure difference compare to a traditional Website

The first and biggest difference in a Single-page-application (SPA) compared to  
a traditionnal Website is that in SPA, the user receives an almost blank HTML, a CSS
and a Js file that will dynamically create all the webpage as the user is using the website. 
While in traditionnal websites, the server generates the HTML 'on the fly' and sends a 
complete HTML CSS Js to the user, this approach basically makes SPAs applications.

This makes hosting cheaper as you don't need a traditionnal server, your Web App
being just a bunch of HTML, CSS, and Js files who doesn't need any 
rendering before being sent to the user. This also allows Hosting in content delivery
network (CDN) and thus better responsiveness. 

While it's usefull to host your Web App, you still need to host the database and 
make it accessible. Then you need to create an API (generaly 
a [REST](https://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask) 
API) so your WebApp (Or anything else using the HTTP protocols) can recover the 
desired information in JSON. 
To create it you can go [serverless](https://serverless-stack.com/chapters/what-is-serverless.html) 
(which I didn't do) or create a more classical server like a Flask app whose sole 
purpose is to provide access to the database throught a REST API.

The two main points to remember is that **your Client-side (an SPA) is basically 
an application** 
and that **the Client-side and Server-side of your 
Web App are totally separated**. If you decide to entierly rewrite your Server-side
while keeping the same API, it's not going to affect the Client-side.

### Android and Desktop Applications
Using framework like React add a big advantages, the presence of pre-configured 
[service-worker](https://developers.google.com/web/fundamentals/primers/service-workers).
allowing a faster WebApp as element of it are cached for subsequent use.

With that comes the best part, the ability to transform your WebApp in an 
[Android](https://codelabs.developers.google.com/codelabs/your-first-pwapp/#0)
application, or an [Desktop](https://developers.google.com/web/progressive-web-apps/desktop)
application. IOS application are also possible if you have 
a Mac, an Iphone and 100€/year to be thrown out the window to publish your
App on the IOS App Store.

### React
The Web App was developed using React Hooks. This is a new version of React implemented
this year, changing the way of developing new components in React, reducing 
the amount of boilerplate code, while being compatible with older versions.

The main points of React are:
* You never touch the DOM
* The website actualize itself when the data changes
* Write pretty [ES6+](https://www.w3schools.com/js/js_es6.asp) Js and JSX code transpiled 
  to old version of Js for compatibility (see link below)

To have a brief but good look at the React syntax and logic I highly recommend this 
[article](https://dev.to/chrisachard/learn-react-in-10-tweets-with-hooks-59bc).
It's the best introduction/explanation of React code in small format I have 
encountered.

#### Web App folder and code structure

Here are a representation of the client/ folder:
```
.
├── package.json
├── package-lock.json
├── public
│   ├── favicon.ico
│   ├── index.html
│   ├── logo192.png
│   ├── logo512.png
│   ├── manifest.json
│   └── robots.txt
├── README.md
└── src
    ├── components
    │   ├── ApiContext.js
    │   ├── Home.jsx
    │   ├── Loading.css
    │   ├── Loading.jsx
    │   ├── Navbar.jsx
    │   ├── Page404.jsx
    │   ├── Phenotype.jsx
    │   ├── PhenotypeList.jsx
    │   ├── SearchPhenotype.jsx
    │   ├── SearchSNP.jsx
    │   ├── SNP.jsx
    │   └── SNPList.jsx
    ├── index.css
    ├── index.jsx
    ├── logo.svg
    └── serviceWorker.js
```

The important files, and folders are:
* package.json: Defines for npm the name of the application, its version, 
 its dependencies, and the associated commands (like for how to start the application
 when doing 'npm start')
* public/: This folder contain the almost empty HTML used by React, the icons and 
 the manifest use to configure the setup as an Android App.
* src/index.jsx: Start point of the WebApp, I use it to configure the API hostname 
 (depending of it's a development or production environment), and the routing of the App
 using react-router package.
* src/components/: Folder containing all the pages and components. Technically, 
 they're the same, it's just their utilisation that differ. The components are used 
 in other pages like NavBar.jsx or Loading.jsx, while Home.jsx is defined by the router
 to be the component that is called when being at '/' url, which still means it's used inside
 the App function of index.jsx.


### Flask
The API is written in Flask because of its simple syntax, and because I like it.
It's deployed on pythonanywhere for its free plan, at the cost of a SLOOOOOOOWWWWW
connection.

## Deployment 
[![Netlify Status](https://api.netlify.com/api/v1/badges/cec9ae99-a3c5-4ff9-9003-5b79acbbe3ca/deploy-status)](https://app.netlify.com/sites/relaxed-euclid-954d97/deploys)

To deploy the SNP database we need to do 4 different tasks:
* Download it
* Install the dependencies
* Setup the database 
* Launch the Server (Flask API) and the Client (Web App) separately

Download the Web App and Flask API with:
```bash
git clone https://gitlab.com/Akusem/dw_final_project.git
```

A conda environment is provided to setup the majority of the dependencies, you can install it with:
```bash
cd dw_final_projetct
conda env create -f environment.yml 
```

And activate it with: 
```bash
conda activate pw_zweig
```
### Client 

First we need to setup the node dependencies
```bash
cd client
# Install local dependencies in node_modules for React
npm install
```

Then we can deploy the Web App:

**Development environment**
```bash
# Launch development build of the web app
npm start
```

**Production environment**
```bash
# Build optimize production build
npm run build
# install 'serve', a production server globally
npm i -g serve
# deploy Web App
serve -S build
```
### Server

To setup the server we first need to create the database:
```bash
cd server
# As the flask app is not named app.py we need to define FLASK_APP
export FLASK_APP=server.py 
# Init database
flask db init
# Creation of migration scripts in server/migration
flask db migrate
# Execute the migration scripts
flask db upgrade 
# Load the database. Use the tsv file as first argument (Can take few minute)
python loadDB.py GWAS_File.tsv
```

After you need to launch the Flask API:

**Development environment**
```bash
# In the conda env
# Development environment
python server.py
```

**Production environment**
```bash
# Install gunicorn
pip install gunicorn
# Launch server
gunicorn server:app
```

## Installation on Android 

SNP database being a Progressive Web App, it's also a mobile Application.
It can be packaged to an APK or an IOS application to be downloadable on the 
Play/App store.

On Android it can be installed from your browser directly by hitting 
and validating "Add SNP DB ont the homescreen" :

![](images/Install_Android_Chrome.jpg)

It will now appear like a native application on your Android homescreen like that:

![](images/Install_android.jpg)

And as an application when using it, not as a website with an address bar: 

![](images/In_App.jpg)


## Author
* Nathanaël Zweig 