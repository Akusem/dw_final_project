import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import * as serviceWorker from './serviceWorker';

import './index.css';
import SNP from './components/SNP';
import Home from './components/Home';
import Navbar from './components/Navbar';
import SNPList from './components/SNPList';
import Page404 from './components/Page404';
import Phenotype from './components/Phenotype';
import ApiContext from './components/ApiContext';
import PhenotypeList from './components/PhenotypeList';


function App() {

  // Define the API Hostname to use depending of if we are in development or production
  let apiHostName;
  if(process.env.NODE_ENV === 'development') {
    apiHostName = 'http://127.0.0.1:5000';
  } else if(process.env.NODE_ENV === 'production') {
    apiHostName = 'https://akusem.pythonanywhere.com';
  }

  return (
    // Provide apiHostName to all component through context
    <ApiContext.Provider value={apiHostName}>
      <Router>
        <Navbar />
        {/* Define internal URL of the web app, and the assiociated components  */}
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/phenotypeList/' component={PhenotypeList} />
          <Route path='/phenotype/:id' component={Phenotype} />
          <Route path='/snplist/' component={SNPList} />
          <Route path='/snp/:id' component={SNP} />
          <Route component={Page404} />
        </Switch>
      </Router>
    </ApiContext.Provider>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
