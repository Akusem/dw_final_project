import React from 'react';

import SearchSNP from './SearchSNP';
import SearchPhenotype from './SearchPhenotype';

function Home() {
  return (
    <section className="section">
      <div className="hero is-link is-bold">
        <div className="hero-body">
          <h1 className="title">
            Welcome to SNP database
          </h1>
          <p className="subtitle">
            A Master 2 DLAD project done by N. Zweig in&nbsp;
            <a 
              rel="noopener noreferrer"
              target="_blank"
              href="https://reactjs.org/"
            >
              <strong className="has-text-danger">
                React
              </strong>
            </a>
          </p>
        </div>
      </div>

      <div className="hero is-dark is-bold">
        <div className="section">
          <SearchSNP />
        </div>
      </div>

      <div className="hero" style={{backgroundImage: 'linear-gradient(to bottom right, #272d2d, #a39ba8)'}}>
        <div className="section">
          <SearchPhenotype />
        </div>
      </div>

    </section>
    
  )
}

export default Home;