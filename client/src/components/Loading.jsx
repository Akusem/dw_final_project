import React from 'react';
import './Loading.css';

function Loading() {
  return (
    <div className="has-text-centered">
      {/* <div className="lds-hourglass"></div> */}
      <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
      {/* <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div> */}
      {/* <div className="lds-grid"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div> */}
    </div>
  )
}

export default Loading;