import React, { useState, useEffect, useContext } from 'react';
import { Link } from "react-router-dom";

import ReactTable from 'react-table';
import 'react-table/react-table.css';

import Loading from './Loading';
import ApiContext from './ApiContext';
import SearchPhenotype from './SearchPhenotype';

function PhenotypeList(props) {
  const [phenoList, setPhenoList] = useState([]);
  const [emptySearch, setEmptySearch] = useState(false);
  const [noConnection, setNoConnection] = useState(false);
  // Recover API Hostname
  const apiHostName = useContext(ApiContext);

  // function to Fetch Phenotype through the REST api
  async function fetchPhenotype(apiHostName, searchState) {
    // Define the url to correspond to a research or to all Phenotype
    const api = searchState === undefined ?
      `${apiHostName}/api/phenotype/` :
      `${apiHostName}/api/phenotypeSearch/${searchState.value}`

    // Init variable
    let res;
    let json;
    let error;
    // Try to contact API up to 5 times, waiting 2s between each attempt
    for (let i = 0; i < 5; i++) {
      try {
        // Make fetch request
        res = await fetch(api);
        // Recover the JSON
        json = await res.json();
        break;
      } catch (err) {
        error = err;
      }
      // Wait 2s
      await new Promise(resolve => setTimeout(resolve, 2000));
    }

    // Test if it recover json, If not set noConnection to display error message
    if (json === undefined) {
      setNoConnection(true);
      throw error;
    }  else if ("empty" in json) {
      // If research result is empty, set emptySearch to true, to print no result. 
      setEmptySearch(true);
      // Reset PhenoList
      setPhenoList([]);
    } else {
      // If result in json, set PhenoList to print the table
      setPhenoList(json);
    }
  };
        
  // Fetch data when the component is render 
  useEffect(() => {
    fetchPhenotype(apiHostName, props.location.state);
  }, [apiHostName, props]);

  // Define the Header and the corresponding value in data for react-table
  const columns = [{
    Header: 'Detail Page',
    accessor: 'Id',
    Cell: props => <Link to={`/phenotype/${props.value}`} className="button is-primary">X</Link>
  }, {
    Header: 'Phenotype',
    accessor: 'Disease_trait',
    width: 400
  }]

  return (
    <div>
      <div className="section">
        <SearchPhenotype />
      </div>
      <div className="level">
        <div className="level-item">
          { // Verify if phenotypeList have been loaded, if not print Loading 
            phenoList.length > 0 ? (
              <div className="table-container">
                <ReactTable
                  className="table has-text-centered"
                  data={phenoList}
                  columns={columns}
                />
              </div>
            ) : noConnection === true ? (
              // Display error message if there is a problem with the internet connection
              <p className="is-size-5">We cannot access to our server, maybe a problem with your internet connection ?</p>
            ) : emptySearch === true ? (
              // Test if the reason for the empty phenoList is due to empty result, if so print no result
              <h2>No result for your research</h2>
              ) : (
              <Loading /> 
            )
          }
        </div>
      </div>
    </div>
  )
};

export default PhenotypeList;