import React, { useState } from 'react';
import { Redirect, Link } from 'react-router-dom';

function SearchPhenotype() {
  const [input, setInput] = useState("");
  const [toRedirect, setToRedirect] = useState(false);

  const handleSearch = () => {
    setToRedirect(true);
  }

  return(
    <div>
      { // if Redirect is true, Redirect to snplist with the search info
        toRedirect === true && 
        <Redirect to={{
            pathname: '/phenotypeList/',
            state: {value: input}
          }}
        /> 
      }
      <label className="label title is-5 has-text-centered">Search <Link to="/phenotypeList/" className="has-text-danger">Phenotype</Link></label>
      <div className="field has-addons">
    
        <div className="control is-expanded">
          <input 
            className="input has-background-dark has-text-white" 
            type="text"
            value={input}
            onChange={event => setInput(event.target.value)}
            placeholder="Search Phenotype"/>
        </div>
    
        <div className="control">
         <button type="submit" onClick={handleSearch} className="button is-danger">Search</button>
        </div>
    
      </div>
    </div>
  )
}

export default SearchPhenotype;