import React from 'react';
import { Link } from 'react-router-dom';

function Page404() {
  return (
    <div className="section">
      <h1 className="title is-size-1 has-text-centered">
        Error 404

      </h1>
      <h4 className="title has-text-centered is-size-5">
        Return to <Link to="/">Home Page</Link>
      </h4>
    </div>
  )
}

export default Page404;