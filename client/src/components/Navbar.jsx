import React, { useState } from 'react';
import { Link } from 'react-router-dom';

function Navbar() {

  // State to contain status of the navbar (active or inactive) 
  const [navBarStatus, setNavBarStatus] = useState('');
  // Function that change the active status of the navbar when a click on the hamburger occur
  const activeNavBar = event => {
    if (navBarStatus === '')
      setNavBarStatus('is-active');
    else 
      setNavBarStatus('');
  }

  return (
    <nav className="navbar">
      <div className="navbar-brand">
        <Link className="navbar-item" to='/'>SNP Database</Link>
        <p role="button" onClick={activeNavBar} className={`${navBarStatus} navbar-burger burger`} aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </p>
      </div>
      <div className={`${navBarStatus} navbar-menu`}>
        <div className="navbar-start">
          
          <div className="navbar-item">
            <Link onClick={activeNavBar} to='/'>
              Home
            </Link>
          </div>

          <div className="navbar-item">
            <Link onClick={activeNavBar} to='/phenotypeList/'>
              Phenotype List
            </Link>
          </div>

          <div className="navbar-item">
            <Link onClick={activeNavBar} to='/snplist/'>
              SNP List
            </Link>
          </div>

        </div>
      </div>
    </nav>
  );
}

export default Navbar;
