import React, { useState } from 'react';
import { Redirect, Link } from 'react-router-dom';


function SearchSNP() {
  const [input, setInput] = useState("");
  const [select, setSelect] = useState("Chrom");
  const [toRedirect, setToRedirect] = useState(false);

  // On submit redirect to snplist by setting toRedirect to true
  const handleSearch = () => {
    setToRedirect(true);
  };

  return (
    <div>
      { // if Redirect is true, Redirect to snplist with the search info
        toRedirect === true && 
          <Redirect to={{
              pathname: '/snplist/',
              state: {field: select, value: input}
            }}
          /> 
      }
      <label className="label title is-5 has-text-centered">Search <Link to="/snplist/" className="has-text-danger">SNPs</Link></label>
      <div className="field has-addons">

        <div className="control">
          <span className="select" style={{backgroundColor: '#FFFFFF', color: '#ECF0DE'}}>
            <select className="is-primary" value={select} onChange={e => setSelect(e.target.value)} id="select_field">
              <option value="Chrom">Chromosome</option>
              <option value="Rsid">Rsid</option>
              <option value="Region">Region</option>
              <option value="Context">Context</option>
            </select>
          </span>
        </div>

        <div className="control is-expanded">
          <input 
            className="input has-background-dark has-text-white" 
            type="text"
            value={input}
            onChange={event => setInput(event.target.value)}
            placeholder="Search SNP"/>
        </div>

        <div className="control">
          <button type="submit" onClick={handleSearch} className="button is-danger">Search</button>
        </div>

      </div>
    </div>
  )
}

export default SearchSNP;
