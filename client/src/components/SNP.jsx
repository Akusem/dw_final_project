import React, { useState, useEffect, useContext } from 'react';
import { useParams, Redirect } from 'react-router';
import { Link } from 'react-router-dom';

import ApiContext from './ApiContext';

export default function SNP() {
  // Recover ID from the URL
  const { id } = useParams();
  // Recover API Hostname
  const apiHostName = useContext(ApiContext);
  const [SNP, setSNP] = useState([]);
  const [to404, setTo404] = useState(false);

  // function to Fetch all info corresponding to the SNP through the REST api
  async function fetchAllInfoSNP(id, apiHostName) {
    // Make fetch request
    const res = await fetch(`${apiHostName}/api/snp/${id}`).catch();
    // Test if the page correspond to a 404
    if (res.status === 404) {
      // Redirect to 404
      setTo404(true);
    } else {
      // Read and Set data to be accessible
      const json = await res.json();
      setSNP(json);
    }
  };

  // Fetch data  when the component is render 
  useEffect(() => {
    fetchAllInfoSNP(id, apiHostName);
  }, [id, apiHostName]);


  return (
    <div className="section">
      {// Redirect to 404 if the SNP id is incorrect
        to404 === true && 
          <Redirect to="/404" />
      }
      <div className="columns">
        <div className="column">
          <h3 className="title is-size-2 has-text-centered">SNP</h3>
          <hr/>

          <p className="has-text-grey">NCBI SNP Id:</p>
          <div className="has-text-centered">
            {/* // Make hyperlink to NCBI SNP page */}
            <a
              rel="noopener noreferrer" target="_blank"
              href={'https://www.ncbi.nlm.nih.gov/snp/'+SNP.Rsid}
            >
              {SNP.Rsid}
            </a>
          </div>
          <p className="has-text-grey">Chromosome: </p>
          <p className="has-text-centered">{SNP.Chrom}</p>
          <p className="has-text-grey">Position: </p>
          <p className="has-text-centered">{SNP.Chrom_pos}</p>
          <p className="has-text-grey">Region: </p>
          <p className="has-text-centered">{SNP.Region}</p>

  {
    Object.keys(SNP).length > 0 &&
      SNP.snp.map((s, i) => {
        return (
          <div key={i}>
            <hr/>
            <p className="is-size-3 has-text-centered has-text-grey">Associated Phenotype {SNP.snp.length > 1 ? i+1 : ''}:</p>
            <div className="has-text-centered">
              <Link to={'/phenotype/'+s.Dis_id}>{s.Disease_name}</Link>
            </div>
            <p className="has-text-grey">P-value: </p>
            <p className="has-text-centered">{s.Pvalue}</p>
            <p className="has-text-grey">NegLog10Pvalue: </p>
            <p className="has-text-centered">{s.NegLog.toFixed(2)}</p>
            <p className="has-text-grey">Risk frequency: </p>
            <p className="has-text-centered">{s.Risk_frequency}</p>
            <p className="has-text-grey">Risk Allele: </p>
            <p className="has-text-centered">{s.Strongest_SNP_risk_allele}</p>
            <p className="has-text-grey">Context: </p>
            <p className="has-text-centered">{s.Context.replace('_variant', '')}</p>
            <br/>
            <p className="has-text-grey">Title:</p>
            <div className="has-text-centered">
              <a 
                rel="noopener noreferrer" target="_blank" 
                href={'https://www.ncbi.nlm.nih.gov/pubmed/'+s.Pubmed_id}
              >
                {s.Title}
              </a>
            </div>
            <p className="has-text-grey">Author:</p>
            <p className="has-text-centered">{s.First_author}</p>
            <p className="has-text-grey">Journal:</p>
            <p className="has-text-centered">{s.Journal}</p>
            <p className="has-text-grey">Sample: </p>
            <p className="has-text-centered">{s.Sample}</p>
          </div>
        )
      })
  }

        </div>
      </div>
    </div>
  )
}

