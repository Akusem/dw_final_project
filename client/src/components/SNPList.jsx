import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';

import ReactTable from 'react-table';
import 'react-table/react-table.css';

import Loading from './Loading';
import SearchSNP from './SearchSNP';
import ApiContext from './ApiContext';


function SNPList(props) {
  const [SNPList, setSNPList] = useState([]);
  const [emptySearch, setEmptySearch] = useState(false);
  const [noConnection, setNoConnection] = useState(false);
  // Recover API Hostname
  const apiHostName = useContext(ApiContext);

  // function to Fetch SNP through the REST api
  async function fetchSNPList(searchState, apiHostName) {
    // Define the url to correspond to a research or to all SNPs
    const apiURL = searchState === undefined ? 
      `${apiHostName}/api/snp/` :
      `${apiHostName}/api/snpSearch/${searchState.field}/${searchState.value}`;

    // Init variable
    let res;
    let json;
    let error;
    // Try to contact API up to 5 times, waiting 2s between each attempt
    for (let i=0; i < 5; i++) {
      try {
        // Make fetch request
        res = await fetch(apiURL);
        // Recover the JSON
        json = await res.json();
        break;
      } catch(err) {
        error = err;
      }
      // Wait 2s
      await new Promise(resolve => setTimeout(resolve, 2000));
    }

    // Test if it recover json, If not set noConnection to display error message
    if (json === undefined) {
      setNoConnection(true);
      throw error
    } else if ('empty' in json) {
      // If research result is empty, set emptySearch to true, to print no result. If not, set SNPList to print the table
      setEmptySearch(true);
      // Reset SNPList
      setSNPList([]);
    } else {
      setSNPList(json);
    }
  };

  // Fetch data  when the component is render 
  useEffect(() => {
    fetchSNPList(props.location.state, apiHostName);
  }, [props, apiHostName]);

  // Define the Header and the corresponding value in data for react-table
  const columns = [{
    Header: 'Detail page',
    accessor: 'Id',
    Cell: props => <Link to={`/snp/${props.value}`} className="button is-primary">X</Link>
  }, {
    Header: 'Rsid',
    accessor: 'Rsid',
  }, {
    Header: 'Associated phenotype',
    accessor: 'Phenotype',
    width: 175
  }, {
    Header: 'P-Value',
    accessor: 'Pvalue',
  }, {
    Header: 'NegLog10',
    accessor: 'NegLog',
    Cell: props => <p>{props.value.toFixed(3)}</p>
  }, {
    Header: 'Chrom',
    accessor: 'Chrom',
    // Check if the value is None, If so replace the value by a N/A string
    Cell: props => <p>{typeof props.value === 'string' ? props.value : 'N/A'}</p>,
    // Sort the Chrom in the right order
    sortMethod: (a, b) => Number(a)-Number(b)
  }, {
    Header: 'Position',
    accessor: 'Chrom_pos',
    Cell: props => <p>{typeof props.value === 'string' ? props.value : 'N/A'}</p>,
    sortMethod: (a, b) => Number(a)-Number(b)
  }, {
    Header: 'Context',
    accessor: 'Context',
    Cell: props => <p>{typeof props.value === 'string' ? props.value : 'N/A'}</p>,
    width: 150
  }, {
    Header: 'Strongest SNP',
    accessor: 'Strongest_SNP_risk_allele'
  }, {
    Header: 'Risk frequency',
    accessor: 'Risk_frequency',
    Cell: props => <p>{typeof props.value === 'string' ? props.value : 'N/A'}</p>
  }, {
    Header: 'Region',
    accessor: 'Region',
    Cell: props => <p>{typeof props.value === 'string' ? props.value : 'N/A'}</p>
  }]


  return (
    <div>
      <div className="section">
        <SearchSNP />
      </div>
      <div className="level">
        <div className="level-item">
          { // Verify if SNPList have been loaded, if not print Loading 
            SNPList.length > 0 ? (
              <div className="table-container">
                <ReactTable
                  className="table has-text-centered"
                  data={SNPList}
                  columns={columns}
                />
              </div>
            ) : noConnection === true ? (
              // Display error message if there is a problem with the internet connection
              <p className="is-size-5">We cannot access to our server, maybe a problem with your internet connection ?</p>
            ) : emptySearch === true ? (
              // Test if the reason for the empty SNPList is due to empty result, if so print no result
              <h2>No result for your research</h2>
              ) : (
              <Loading /> 
            )
          }
        </div>
      </div>
    </div>
  )
}

export default SNPList;