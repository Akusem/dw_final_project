import React, { useState, useContext, useEffect } from "react";
import { useParams, Redirect, Link } from "react-router-dom";

import ApiContext from "./ApiContext";

function Phenotype() {
  // Recover ID from the URL
  const { id } = useParams();
  // Recover API Hostname
  const apiHostName = useContext(ApiContext);
  const [pheno, setPheno] = useState([]);
  const [to404, setTo404] = useState(false);

  // function to Fetch all info corresponding to the Phenotype through the REST api
  async function fetchPhenotype(apiHostName, id) {
    // Make fetch request
    const res = await fetch(`${apiHostName}/api/phenotype/${id}`).catch();

    // Test if the page correspond to a 404
    if (res.status === 404) {
      // Redirect to 404
      setTo404(true);
    } else {
      // Read and Set data to be accessible
      const json = await res.json();
      setPheno(json);
    }
  }

  // Fetch data  when the component is render
  useEffect(() => {
    fetchPhenotype(apiHostName, id);
  }, [apiHostName, id]);

  return (
    <div className="section">
      {// Redirect to 404 if the SNP id is incorrect
      to404 === true && <Redirect to="/404" />}
      <div className="columns">
        <div className="column">
          <h3 className="title is-size-2 has-text-centered">Phenotype</h3>
          <hr />
          <p className="has-text-grey">Definition: </p>
          <p className="has-text-centered">{pheno.Name}</p>
          <p className="has-text-grey">Associated Article: </p>
          <ul className="has-text-centered">
            {// Verify if pheno have been loaded
            Object.keys(pheno).length > 0 &&
              pheno.Article.map((article, i) => {
                return (
                  <li key={i}>
                    <a
                      rel="noopener noreferrer"
                      target="_blank"
                      href={
                        "https://www.ncbi.nlm.nih.gov/pubmed/" +
                        article.PubmedId
                      }
                    >
                      {article.Title} 
                    </a>
                  </li>
                );
              })}
          </ul>
          <p className="has-text-grey">Associated SNP: </p>
          <ul className="has-text-centered">
            {// Verify if pheno have been loaded
            Object.keys(pheno).length > 0 &&
              pheno.Snp.map((snp, i) => {
                return (
                  <li key={i}>
                    <Link to={"/snp/" + snp.Id}>{snp.Rsid}</Link>
                  </li>
                );
              })}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Phenotype;
